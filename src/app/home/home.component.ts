import { Component, OnInit,HostListener } from '@angular/core';
import {Router} from "@angular/router";
import {CommonService} from '../_services/common.service'
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
/* @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
  }*/  
  cities;  
  doctor_speciality;
  input_speciality;
  input_location;
  constructor(private commonService:CommonService,
              private router: Router
  ) { 
    this.commonService.available_cities().subscribe(res=>{
      this.cities = res.data
    })
    this.commonService.doctor_speciality().subscribe(res=>{
      this.doctor_speciality = res.data
    })
  }
  onSearchChange(eventKey){
    if(eventKey == 'Enter'){
      this.router.navigate(['/search', this.input_location,this.input_speciality]);

    }
  }
  ngOnInit() {
      if ($.fn.owlCarousel) {
          var welcomeSlide = $('.hero-slides');
          $('.hero-slides').owlCarousel({
              items: 1,
              margin: 0,
              loop: true,
              nav: false,
              navText: ['Prev', 'Next'],
              dots: false,
              autoplay: true,
              autoplayTimeout: 5000,
              smartSpeed: 1000
          });
      }
   
  }
 	
}
