import { Component,NgModule, OnInit,Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

import {UserService} from '../_services/user.service'
import {CommonService} from '../_services/common.service'
declare var $ :any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user;
  constructor(private toastr: ToastrService,
    private userService: UserService,
    private commonService: CommonService,
    private router:Router,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    ) {
   }

  ngOnInit() {
    this.user = this.storage.get('user');
    /*console.log(this.user)*/
  }
  signIn(frmValues):void {
    this.userService.getLogin(frmValues)
      .subscribe(
        res => {
        $("#SignIn").modal("hide")
        this.storage.set('user', res.data);
        this.user = res.data;
        this.toastr.success('You are successfully logged in.');
    },
    err => {
      console.log(err.error.data)
      this.toastr.error(err.error.data);
    } 
      );      
    console.log(frmValues)
  }
  
  signUp(frmValues):void{
 	console.log(frmValues)
  }
}
