import { Component, OnInit,Inject } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

import {UserService} from '../_services/user.service'
import {CommonService} from '../_services/common.service'


@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.css']
})
export class BookAppointmentComponent implements OnInit {
  selected_time = "0"
  selected_date = "0"
  constructor(
  	private activatedRoute: ActivatedRoute,
  	  	private userService: UserService,
  	private commonService: CommonService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,


  	) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
		var input;
	  input = {type:[params['type']],id:params['id'] }
	  console.log(input)
/*	  this.userService.hospital_list(input).subscribe(res=>{
	    this.list_data['hospitals'] = res.data;
	    console.log(this.list_data,'t');

	    })
	  this.userService.doctor_list(input).subscribe(res=>{
	    this.list_data['doctors'] = res.data;

	  })*/

    });
  }

}
