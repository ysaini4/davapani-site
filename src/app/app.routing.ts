import { Routes } from '@angular/router';
import { SearchResultComponent }   from './search-result/search-result.component';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent }   from './home/home.component';
import { CsrComponent }   from './csr/csr.component';
import { ServicesComponent }   from './services/services.component';

import { PanelComponent } from './panel/panel.component';
import { MissionComponent } from './mission/mission.component';
import { VisionComponent } from './vision/vision.component';
import { WhyChooseUsComponent } from './why-choose-us/why-choose-us.component';
import { HospitalsComponent } from './hospitals/hospitals.component';
import { OrderSuccessComponent } from './order-success/order-success.component';


import { LaboratoryComponent } from './laboratory/laboratory.component';

import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsCondationsComponent } from './terms-condations/terms-condations.component';
import { FaqComponent } from './faq/faq.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',

    },
     {
        path: 'search/:location/:speciality',
        component: SearchResultComponent,
        
    },
    {
        path: 'appointment/:type/:id',
        component: BookAppointmentComponent,
        
    },
	{
        path: 'home',
        component: HomeComponent,
        
    },
    {
        path: 'labs',
        component: HomeComponent,
        
    },
    {
        path: 'about',
        component: AboutComponent,
        
    },
    {
        path: 'csr',
        component: CsrComponent,
        
    },
    {
        path: 'services',
        component: ServicesComponent,
        
    },
    {
        path: 'panel/:type/:id',
        component: PanelComponent,
        
    },
    {
        path: 'mission',
        component: MissionComponent,
        
    },
     {
        path: 'vision',
        component: VisionComponent,
        
    },
    {
        path: 'vision',
        component: VisionComponent,
        
    },
     {
        path: 'whychooseus',
        component: WhyChooseUsComponent,
        
    },
     {
        path: 'hospitals',
        component: HospitalsComponent,
        
    },
    {
        path: 'Laboratory',
        component: LaboratoryComponent,
        
    },
     {
        path: 'privacyPolicy',
        component: PrivacyPolicyComponent,
        
    },
     {
        path: 'termsCondations',
        component: TermsCondationsComponent,
        
    },
    {
        path: 'faq',
        component: FaqComponent,
        
    },
     {
        path: 'success',
        component: OrderSuccessComponent,
        
    }
];