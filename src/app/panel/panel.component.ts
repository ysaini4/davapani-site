import { Component, OnInit,NgModule,Inject} from '@angular/core';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';
import { ToastrService } from 'ngx-toastr';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../_services/user.service';
import {CommonService} from '../_services/common.service';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  selectDate = 0
  selectTime = 0
  timeslot = {date:[],time:[]};
  userInfo;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '10rem',
    minHeight: '15rem',
    placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
    translate: 'no'
  };
  updateable;

 constructor(private toastr: ToastrService,
    private userService: UserService,
    private commonService: CommonService,
    private router:Router,
    private activatedRoute: ActivatedRoute,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    ) { }
  ngOnInit() {

    this.timeslotManagment();
      this.activatedRoute.params.subscribe((params: Params) => {
      var input;
      input = {type:params['type'],id:params['id'] }
      if(params['type'] == 'user'){
        this.updateable = true;
      }
        this.userInfo = this.storage.get('user');
    });   

  }
    public timeslotManagment():void{
	  	var today = new Date();
	    var date = Array();
	    for(var i=0;i<7;i++){
	        var nextday = new Date();
	        nextday.setDate(today.getDate()+i);
	        date.push(this.getDayByDate(nextday))
	    }
	    this.timeslot.date = date;
	    let time = Array()
	    for(var i=0;i<48;i++){
		    var nexttime = new Date();
		    nexttime.setTime(today.getTime() + (i*60*30*1000)); 
		    var timeHR,timeMIN;
		    var hr = nexttime.getHours();
		    var min = nexttime.getMinutes();
		    if(min>=30){
		      timeMIN = '30';
		    } else {
		      timeMIN = '00';
		    }
	      	var mod;
	      	if(hr>12){
	        	timeHR = (nexttime.getHours())%12;
	        	mod = 'PM'
	      	}else if(hr==0){
	        	timeHR = 12;
	        	mod = 'AM'
	      	}
	      	else {
	        	timeHR = (nexttime.getHours());
	        	mod = 'AM'
	        if(timeHR == 12)
	          	mod = 'PM'
	      }
	      if(hr<=19 && hr>=7){
		      var c_time = timeHR+':'+timeMIN+' '+mod;
			  time.push(c_time)
	      }
  		}
  		this.timeslot.time = time;
	}
    public getDayByDate(date):string{
    var weekday = new Array(7);
    weekday[0] =  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    return weekday[date.getDay()]+ ' ('+date.getDate()+' '+month[date.getMonth()]+')'
 }
  UpdateProfile(){
  	
  	this.userService.doctorSpeciality().subscribe(
  			 res => {
                  this.dropdownList = res.data; 
                },
  		)  

  	 this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 8,
      allowSearchFilter: false
    };

  };


  url: string;
onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
       if(event.target.result)
         this.url = event.target.result;
      }
    }
}
 
  public ProfileUpdate(input,id):void{  

    var data = {
    	_id:id,
    	"update_data":{
	        "name":{
	          "first":input.first,
	          "last": input.last,
	        },
	        "username":input.username,
	        "password":input.password,
	        "mobile":input.mobile,
	        "speciality":input.speciality,
	        "city":input.city,
	        "state":input.state,
	        "email":input.email,        
	        "address_line":input.address_line,
	        "zip_code":input.postalcode,
	        "about":input.about,
       		 "country":input.country
   	 }
    }
    
    this.userService.updateprofile(data)
      .subscribe(
          res => {
              if(res.status){
                this.toastr.success(res.data)
              }
          },
          err => {
              this.toastr.error(err.data,'danger')
          } 
      );      
  }
}
