import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutes } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StorageServiceModule} from 'angular-webstorage-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';
import { AboutComponent } from './about/about.component';
import { CsrComponent } from './csr/csr.component';
import { ServicesComponent } from './services/services.component';
import { PanelComponent } from './panel/panel.component';
import { VisionComponent } from './vision/vision.component';
import { MissionComponent } from './mission/mission.component';
import { WhyChooseUsComponent } from './why-choose-us/why-choose-us.component';
import { HospitalsComponent } from './hospitals/hospitals.component';
import { LaboratoryComponent } from './laboratory/laboratory.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsCondationsComponent } from './terms-condations/terms-condations.component';
import { FaqComponent } from './faq/faq.component';
import { OrderSuccessComponent } from './order-success/order-success.component';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SearchResultComponent,
    BookAppointmentComponent,
    AboutComponent,
    CsrComponent,
    ServicesComponent,
    PanelComponent,
    VisionComponent,
    MissionComponent,
    WhyChooseUsComponent,
    HospitalsComponent,
    LaboratoryComponent,
    PrivacyPolicyComponent,
    TermsCondationsComponent,
    FaqComponent,
    OrderSuccessComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    StorageServiceModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    ToastrModule.forRoot({positionClass: 'toast-bottom-right',progressBar:false}) 
  ],
  providers: [
 {provide: LocationStrategy, useClass: HashLocationStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
