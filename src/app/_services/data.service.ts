import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of,BehaviorSubject } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable()
export class DataService {
  	private messageSource = new BehaviorSubject('default message');
  	currentMessage = this.messageSource.asObservable();
  	constructor() { }
 	changeMessage(message: string) {
    	this.messageSource.next(message)
  	}	
}
