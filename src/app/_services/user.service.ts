import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';


import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
	serverUrl = environment.serverUrl

  constructor(private http: HttpClient) { }
	getLogin(inputs):Observable<getLogin> {
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"login";
  		let data = {
			"data":inputs
		}
  		return this.http.post<getLogin>(url,data,httpOptions);
	}
	doctor_list(inputs):Observable<doctor_list>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
	};
		var data = {
			data:inputs
		}
  		let url = this.serverUrl+"doctor_list";
  		return this.http.post<doctor_list>(url,data,httpOptions);
	}
	hospital_list(inputs):Observable<hospital_list>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
	};
		var data = {
			data:inputs
		}
		console.log(data)
  		let url = this.serverUrl+"hospital_list";
  		return this.http.post<hospital_list>(url,data,httpOptions);
	}

	doctorSpeciality():Observable<speciality>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"doctor_speciality";
  		return this.http.get<speciality>(url,httpOptions);

	}
	
	updateprofile(data):Observable<up> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"updatedoctor";
  		return this.http.post<up>(url,body,httpOptions);
	}
}
interface up{
	status:boolean,
	data:string
}
interface speciality{	
	status:boolean,
	data:[string]
}
interface getLogin  {
	status:boolean,
	app:string,
	auth:string,
	data:{token:string,userid:string},
}
interface doctor_list {
	status:boolean,
	app:string,
	data:[string]
}
interface hospital_list {
	status:boolean,
	app:string,
	data:[string]
}
