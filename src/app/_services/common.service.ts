import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';


import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
	serverUrl = environment.serverUrl

  constructor(private http: HttpClient) { }
	available_cities():Observable<available_cities>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
	};
  		let url = this.serverUrl+"available_cities";
  		return this.http.get<available_cities>(url,httpOptions);
	}
	doctor_speciality():Observable<doctor_speciality>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
	};
  		let url = this.serverUrl+"doctor_speciality";
  		return this.http.get<doctor_speciality>(url,httpOptions);
	}
}
interface available_cities {
	status:boolean,
	app:string,
	data:[string]
}
interface doctor_speciality {
	status:boolean,
	app:string,
	data:[string]
}