import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsCondationsComponent } from './terms-condations.component';

describe('TermsCondationsComponent', () => {
  let component: TermsCondationsComponent;
  let fixture: ComponentFixture<TermsCondationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsCondationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsCondationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
