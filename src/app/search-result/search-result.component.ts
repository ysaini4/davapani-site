import { Component, OnInit,Inject} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

import {UserService} from '../_services/user.service'
import {CommonService} from '../_services/common.service'

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  list_data = Array();
//  this.list_data = {doctors:{},hospitals:{}}
  constructor(
  	private activatedRoute: ActivatedRoute,
  	private userService: UserService,
  	private commonService: CommonService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService,

  	) { }
  ngOnInit() {
  	// subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
    	var input;
      //input = {speciality:[params['speciality']],city:params['location'] }
      this.userService.hospital_list(input).subscribe(res=>{
        this.list_data['hospitals'] = res.data;
        this.storage.set('search_hospitals',res.data)
      })
      this.userService.doctor_list(input).subscribe(res=>{
        this.list_data['doctors'] = res.data;
        this.storage.set('search_doctors',res.data)
      })

    });
  }

}
